package hadoop.hadoop_hw1;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import hadoop.hadoop_hw1.*;
import hadoop.hadoop_hw1.BrowserCount.BrowserMapper;
import hadoop.hadoop_hw1.BrowserCount.BrowserReducer;

public class MapperTest {
	
	MapReduceDriver<Object, Text, Text, Text, Text, IntWritable> mapReduceDriver;
	MapDriver<Object, Text, Text, Text> mapDriver;
	ReduceDriver<Text, Text, Text, IntWritable> reduceDriver;
	
	String testIE 		= "123.221.14.56 - - [10/Oct/2013:22:45:14 ] \"GET /shifters HTTP/1.0\" 200 3409 \"-\" \"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)\"";
	String IEValue		= "123.221.14.56";
	String IEKey		= "IE";
	
	String testFirefox	= "81.73.150.239 - - [10/Oct/2013:00:22:39 ] \"GET /shifters HTTP/1.0\" 200 3273 \"http://searchengine.com\" \"Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0\"";
	String firefoxValue	= "81.73.150.239";
	String firefoxKey	= "Firefox";
	
	String testOther	= "198.122.118.164 - - [10/Oct/2013:00:26:06 ] \"GET /forks HTTP/1.0\" 200 4959 \"-\" \"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\"";
	String otherValue	= "198.122.118.164";
	String otherKey		= "Other";
	
	String testOther2	= "81.73.150.239 - - [10/Oct/2013:02:54:49 ] \"GET /forks HTTP/1.0\" 200 2034 \"-\" \"Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201\"";
	String other2Value	= "81.73.150.239";
	String other2Key	= "Other";
	
	String testError	= "81.73.150.239 - dhdj - [10/Oct/2013:02:54:49 ] \"GET /forks HTTP/1.0\" 200 2034 \"-\" \"Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201\"";
	String errorValue	= "81.73.150.239";
	String errorKey		= "Other";
	
	@Before
    public void setUp() {
        BrowserMapper mapper = new BrowserMapper();
        BrowserReducer reducer = new BrowserReducer();
        mapDriver = new MapDriver<Object, Text, Text, Text>();
        mapDriver.setMapper(mapper);
        reduceDriver = new ReduceDriver<Text, Text, Text, IntWritable>();
        reduceDriver.setReducer(reducer);
        mapReduceDriver = new MapReduceDriver<Object, Text, Text, Text, Text, IntWritable>();
        mapReduceDriver.setMapper(mapper);
        mapReduceDriver.setReducer(reducer);
    }
	
	@Test
    public void testMapIE() {
        mapDriver.withInput(new Object(), new Text(testIE));
        mapDriver.withOutput(new Text(IEKey), new Text(IEValue));
        mapDriver.runTest();
    }
	
	@Test
    public void testMapFirefox() {
        mapDriver.withInput(new Object(), new Text(testFirefox));
        mapDriver.withOutput(new Text(firefoxKey), new Text(firefoxValue));
        mapDriver.runTest();
    }
	
	@Test
    public void testMapOther() {
        mapDriver.withInput(new Object(), new Text(testOther));
        mapDriver.withOutput(new Text(otherKey), new Text(otherValue));
        mapDriver.runTest();
    }
	
	@Test
    public void testReduce() {
        System.out.println("\ntestReduce");
        ArrayList<Text> values = new ArrayList<Text>();
        values.add(new Text(IEValue));
        values.add(new Text(IEValue));
        values.add(new Text(firefoxValue));
        values.add(new Text(otherValue));
        reduceDriver.withInput(new Text(IEKey), values);
        reduceDriver.withOutput(new Text(IEKey), new IntWritable(3));
        reduceDriver.runTest();
    }
	
	@Test
	public void testMapReduce() {
        System.out.println("\nMapReduce Testing");
        mapReduceDriver.addInput(new Object(), new Text(testIE));
        mapReduceDriver.addInput(new Object(), new Text(testFirefox));
        mapReduceDriver.addInput(new Object(), new Text(testOther));
        mapReduceDriver.addInput(new Object(), new Text(testOther2));

        mapReduceDriver.addOutput(new Text(firefoxKey), new IntWritable(1));
        mapReduceDriver.addOutput(new Text(IEKey), new IntWritable(1));
        mapReduceDriver.addOutput(new Text(otherKey), new IntWritable(2));

        mapReduceDriver.runTest();
        System.out.println("END");
    }
}
