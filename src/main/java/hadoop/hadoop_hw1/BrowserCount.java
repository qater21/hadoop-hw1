package hadoop.hadoop_hw1;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;


public class BrowserCount {
	/**
    * Mapper class
    */
	public static class BrowserMapper
       extends Mapper<Object, Text, Text, Text>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
    private final Integer numField = 9;
    private int numfile = 0;
    
    /**
     * Process row (apache-log) and return pair Browser - Ip
     * @param key - unused Object
     * @param value - Text value
     * @param context  - context
     * @throws IOException
     * @throws InterruptedException
     */
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String rexIp = "(\\d+(?:\\.\\d+){3})";  // an IP address
      String rexs = "(\\S+)";                // a single token (no spaces)
      String rexdt = "\\[([^\\]]+)\\]";      // something between [ and ]
      String rexstr = "\"([^\"]*?)\"";       // a quoted string
      String rexi = "(\\d+)";                // unsigned integer

      String rex = String.join( " ", rexIp, rexs, rexs, rexdt, rexstr,
                            rexi, rexi, rexstr, rexstr );
      Pattern pat = Pattern.compile( rex );

    	  Matcher mat = pat.matcher(value.toString());
          if( mat.matches() && mat.groupCount() >= numField){
        	  String userAgent = mat.group(numField);
        	  if(userAgent.contains("Firefox") && !userAgent.contains("Seamonkey"))
        		  word.set("Firefox");
        	  else if(userAgent.contains("; MSIE"))
        		  word.set("IE");
        	  else
        		  word.set("Other");
          }
          context.write(word, new Text(mat.group(1)));
          
          //Files.write(new Path("/root/testfile"), word.toString() + mat.group(1) + "\n",new OpenOption());
        }
  }
	
	/**
     * Reducer class
     */
	public static class BrowserReducer
       extends Reducer<Text,Text,Text,IntWritable> {
    private IntWritable result = new IntWritable();
    int numfile = 0;
    
    /**
     * Return pair Browser - number of unique ip adresses
     * @param key
     * @param values
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    public void reduce(Text key, Iterable<Text> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      
      HashSet<String> users = new HashSet<>();
      for (Text val : values) {
        if(!users.contains(val.toString())){
        	sum++;
        	users.add(val.toString());
        	System.out.println(key.toString() + " " + val.toString() + " " + sum + " " + users.size());
        }
      }
      result.set(sum);
      System.out.println("SIZE " + users.size());
      System.out.println(key + " " + Integer.toString(sum));
      context.write(key, result);
    }
  }
	/**
     * Main method
     * @param args : args[0] - input folder, args[1] - output folder
     * @throws Exception
     */
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    
    Job job = Job.getInstance(conf, "Browser count");
    /**
     * Mapper and Reducer config
     */
    job.setJarByClass(BrowserCount.class);
    job.setMapperClass(BrowserMapper.class);
    //job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(BrowserReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);

    FileOutputFormat.setCompressOutput(job,true);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    
    /**
     * Set sequence output
     */
    SequenceFileOutputFormat.setOutputPath( job, new Path(args[1]) );
    SequenceFileOutputFormat.setCompressOutput( job, true );
    SequenceFileOutputFormat.setOutputCompressorClass( job, SnappyCodec.class );
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}